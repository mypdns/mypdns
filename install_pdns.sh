#!/usr/bin/env bash
# exit on any failure during install
set -e

#
# root has to run the script
#
if [ `whoami` != "root" ]
    then
    echo "You need to be root to do this!"
    exit 1
fi

## First we ensure the system packages are up to date
apt-get update
apt-get dist-upgrade

# install packages for pdns, new relic and management (curl vim tmux)
echo "Getting the packages for our DNS setup"
apt-get install lua5.3 luarocks curl wget tmux make ntp libssl-dev dnsutils git build-essential fakeroot devscripts gawk gcc-multilib dpatch -y

## The PowerDNS Authoritative Server depends on Boost, OpenSSL and requires a compiler with C++-2011 support.
apt-get install g++ libboost-all-dev libtool pkg-config default-libmysqlclient-dev libssl-dev virtualenv
apt-get install autoconf automake ragel bison flex

##then generate the configure file:
autoreconf -vi

# luarocks manages library installs for lua, these three are necessary for redis and syslog
luarocks install luasocket
luarocks install json4lua
luarocks install redis-lua
echo "Adding pdns group and user, pulling latest recursor"
addgroup pdns
adduser pdns --shell /bin/false --no-create-home --gecos PowerDNS --ingroup pdns --disabled-login --disabled-password
# delete exiting pdns.list from apt until we can tjek if it exist
rm -f '/etc/apt/sources.list.d/pdns.list'

# installing latest Powerdns | PowerDNS Recursor | PowerDNS dnsdist master build
echo "Installing latest pdns-recursor, checking for debian/ubuntu from master branch"
os=`lsb_release -is`
lsb=`lsb_release -cs`
echo "deb [arch=amd64] http://repo.powerdns.com/{$os,,} {$lsb,,}-auth-master main" > `/etc/apt/sources.list.d/pdns.list`
echo "deb [arch=amd64] http://repo.powerdns.com/{$os,,} {$lsb,,}-rec-master main" >> `/etc/apt/sources.list.d/pdns.list`
echo "deb [arch=amd64] http://repo.powerdns.com/{$os,,} {$lsb,,}-dnsdist-master main" >> `/etc/apt/sources.list.d/pdns.list`

# Add package priority for powerdns, but first delete if exist
rm -f '/etc/apt/preferences.d/pdns'
echo "Package: pdns-*" > '/etc/apt/preferences.d/pdns'
echo "Pin: origin repo.powerdns.com" >> '/etc/apt/preferences.d/pdns'
echo "Pin-Priority: 600" >> '/etc/apt/preferences.d/pdns'

rm -f '/etc/apt/preferences.d/dnsdist'
echo "Package: dnsdist*" > '/etc/apt/preferences.d/dnsdist'
echo "Pin: origin repo.powerdns.com" >> '/etc/apt/preferences.d/dnsdist'
echo "Pin-Priority: 600" >> '/etc/apt/preferences.d/dnsdist'

echo "Let's update the server with apt-get update"
apt-get update

# Since dns-over-tls is enbled by default from version 1.3.2
# we change this to default package installation
echo "Next we install Recursor + dnsdist"
apt-get install -y dnsdist openssl pdns-recursor

# Now add a tmpfs to store the cert for dns-over-tls
echo "tmpfs   /dev/cert        tmpfs   size=10m,noexec,nosuid,nodev,noatime     0 0" >> '/etc/fstab'
mkdir '/dev/cert'
mount '/dev/cert'

#if [ ${MACHINE_TYPE} == 'x86_64' ]; then
#	echo "64 bit detected"
#	wget http://downloads.powerdns.com/releases/deb/pdns-recursor_3.6.1-1_amd64.deb
#	dpkg -i pdns-recursor_3.6.1-1_amd64.deb
#else
#	echo "32 bit detected"
#	wget http://downloads.powerdns.com/releases/deb/pdns-recursor_3.6.1-1_i386.deb
#	dpkg -i pdns-recursor_3.6.1-1_i386.deb
#fi

# get rid of default config files to move in our own
echo "Removing default config files of rsyslog"
rm -f /etc/rsyslog.conf
# move config files & script for our installation
echo "Moving files over!"
mv files-pdns/lua/* /etc/powerdns/
mv files-pdns/recursor.conf /etc/powerdns/
mv files-pdns/rsyslog.conf /etc/
mv files-pdns/pdns.conf /etc/rsyslog.d/
mv files-pdns/rsyslog /etc/logrotate.d/
mv files-pdns/ntp.conf /etc/ntp.conf
echo "Permissions for pdns"
chown pdns-svc:pdns -R /etc/powerdns/
chown pdns-svc:pdns /usr/bin/rec_control
chown pdns-svc:pdns /etc/init.d/pdns-recursor
chown pdns-svc:pdns /usr/sbin/pdns_recursor
mkdir /var/run/pdns-rec
chown -R pdns:pdns /var/run/pdns-rec
chmod 750 /var/run/pdns-rec
chmod 766 /usr/bin/rec_control
echo "Restarting services"
# restart everything
systemctl restart rsyslog
systemctl restart pdns-recursor
echo "Cleanup"
# clean up
apt-get autoremove
# starting pdns-recursor
/etc/init.d/pdns-recursor start
